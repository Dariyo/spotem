<?php

class LocationController {
	public function set($request) {
		$memcache = new Memcache;  
		$memcache->connect('localhost', 11211) or die ("Could not connect"); 
		
		$phoneNumber = $request->getRessourceID();
		$lat = $request->getRequestVars()["lat"];
		$long = $request->getRequestVars()["long"];
		
		$loc = new Location;
		$loc->lat = $lat; 
		$loc->long = $long; 
	
		$memcache->set($phoneNumber, $loc, false, 60);
		$loc = $memcache->get($phoneNumber);
		
		echo "success";
	}

	public function get($request) {
		$memcache = new Memcache;  
		$memcache->connect('localhost', 11211) or die ("Could not connect"); 

		$phones = $request->getRequestVars()["phones"];
		$phoneArr = explode(",", $phones);
		$first = true;
		
		foreach ($phoneArr as $value){
			$loc = $memcache->get($value);
			
			if ($loc){
				if (!$first){
					echo " ";
				}
				$first = false;
				echo $value . ":" . $loc->lat . "," . $loc->long;
			}
		}
	}
}
?>