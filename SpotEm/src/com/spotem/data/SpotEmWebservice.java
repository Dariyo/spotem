package com.spotem.data;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class SpotEmWebservice {
	private static String IP = "http://128.39.169.228";
	private static String SET_URL = IP + "/spotem/location/set/";
	private static String GET_URL = IP + "/spotem/location/get/?phones=";
	
	public static ArrayList<Contact> getLocationFromPhoneNumber(ArrayList<Contact> contacts) {
		HashMap<String, Contact> contactMap = new HashMap<String, Contact>();
		
		String uri = GET_URL;
		for (Contact contact : contacts) {
			contactMap.put(contact.phoneNumber, contact);
			uri += contact.phoneNumber + ",";
		}
		
		uri = uri.substring(0, uri.length() - 1);
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		HttpResponse response;

		contacts.clear();
		try {
			response = httpClient.execute(get);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 404) {
				Log.e("Webservice", "404");
				return null;
			}
			
			String result = EntityUtils.toString(response.getEntity());

			String[] split = result.split("\\s+");
			for (String phoneLoc : split) {
				if (phoneLoc.length() <= 1){
					continue;
				}
				
				String[] splitResult = phoneLoc.split("\\:");
				String phoneNumber = splitResult[0];
				Contact contact = contactMap.get(phoneNumber);
				
				String[] locationString = splitResult[1].split("\\,");
				contact.location.setLat(Double.parseDouble(locationString[0]));
				contact.location.setLong(Double.parseDouble(locationString[1]));
				contacts.add(contact);
				
				Log.e("result", phoneNumber);
			}
		} catch (Exception e) {
			Log.e("Webservice error", e.toString());
			return contacts;
		}
		return contacts;
	}

	public static boolean checkOnline(ConnectivityManager cm) {
		try {
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnected()) {
				URL url = new URL(IP + "/spotem");
				HttpURLConnection urlc = (HttpURLConnection) url
						.openConnection();
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(2000);
				urlc.connect();
				
				//501 = Not implemented  - is expected for the service to be online
				if (urlc.getResponseCode() == 501){ 
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception ex) {
			return false;
		}
		return false;
	}

	public static void setLocation(String phoneNumber, Location loc) {
		phoneNumber = phoneNumber.replace(" ", "");
		phoneNumber = phoneNumber.replace("+", "");
		
		String uri = SET_URL;
		uri += phoneNumber + "/";
		uri += "?lat=" + loc.getLat();
		uri += "&long=" + loc.getLong();

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		try {
			HttpResponse resp = httpClient.execute(get);
			if (!EntityUtils.toString(resp.getEntity()).equals("success")) {
				Log.e("Webservice", "Could not set location");
			}
		} catch (Exception e) {
			Log.w("Webservice", e.getMessage());
		}
	}
}
