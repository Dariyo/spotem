package com.spotem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.spotem.data.Contact;
import com.spotem.data.ContactList;
import com.spotem.data.SpotEmWebservice;
import com.spotem.data.Location;

import android.os.AsyncTask;
import android.widget.TextView;

public class GetLocationTask extends AsyncTask<ArrayList<Contact>, Integer, ArrayList<Contact>>{
	@Override
	protected ArrayList<Contact> doInBackground(ArrayList<Contact>... params) {
		try{
			return SpotEmWebservice.getLocationFromPhoneNumber(params[0]);
		}catch(Exception e){
			return new ArrayList<Contact>();
		}
	}
	
	@Override
	 protected void onPostExecute(ArrayList<Contact> contacts) {
		for (Contact contact : contacts) {
			if (ContactList.contacts.containsKey(contact.phoneNumber)){
				//Already updated by reference
			}else{
				//Not in the list, add now
				ContactList.contacts.put(contact.phoneNumber, contact);
			}
		}
	 }
}
