package com.spotem;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceActivity;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.spotem.data.Contact;
import com.spotem.data.ContactList;

public class ContactListActivity extends FragmentActivity implements
		ContactListFragment.Callbacks, Runnable, UncaughtExceptionHandler{
	private static ContactListFragment contactListFragment;
	private Handler handler = new Handler();
	private static boolean isInitalized = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Thread.setDefaultUncaughtExceptionHandler(this);
		
		super.onCreate(savedInstanceState);
		if (!isInitalized) {
			ContactList.Initalize();
			LocationUpdateManager.instance().init(this);
			handler.postDelayed(this, 0);
			isInitalized = true;
			ServiceAvailabilityTask task = new ServiceAvailabilityTask();
			task.context = this;
			task.execute((ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE));
		}
		
		setContentView(R.layout.activity_contact_list);
		contactListFragment = (ContactListFragment) getSupportFragmentManager()
				.findFragmentById(R.id.contact_list);

		LocationUpdateManager.instance().setSharing(getPreferences(MODE_PRIVATE).getBoolean("shareLocation", false));
	}

	@Override
	public void onItemSelected(String id) {
		Intent detailIntent = new Intent(this, ContactDetailActivity.class);
		detailIntent.putExtra(ContactDetailFragment.ARG_ITEM_ID, id);
		startActivity(detailIntent);
	}

	 @Override
     public boolean onOptionsItemSelected(MenuItem item)
     {
		 Intent detailIntent = new Intent(this, SettingsActivity.class);
		 startActivity(detailIntent);
			
		return true;
     }
	 
	 @Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     super.onCreateOptionsMenu(menu);
	     menu.add(Menu.NONE, 0, Menu.NONE, "Settings");
	     return true;
	 }
	 
	@Override
	public void run() {
		try{
			Cursor phones = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
					null, null);
				
			ArrayList<Contact> contacts = new ArrayList<Contact>();

			while (phones.moveToNext()) {
					String name = phones
								.getString(phones
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				
				String phoneNumber = phones
						.getString(phones
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				phoneNumber = phoneNumber.replace(" ", "");
				phoneNumber = phoneNumber.replace("+", "");

				if (ContactList.contacts.containsKey(phoneNumber)){
					contacts.add(ContactList.contacts.get(phoneNumber));
				}else{
					Contact contact = new Contact(phoneNumber, name);
					contacts.add(contact);
				}
			}
					
			if (contacts.size() > 0){
				GetLocationTask task = new GetLocationTask();
				task.execute(contacts);
			}

			phones.close();
			MoveDummy();
			
			handler.postDelayed(this, 3000);
			contactListFragment.FillContacts();
		}catch(Exception ex){
			handler.postDelayed(this, 3000);
		}
	}

	private void MoveDummy() {
		if (ContactList.INCLUDE_TEST_DUMMIES){
			Contact movingDummy = ContactList.MovingDummy;
			movingDummy.location.setLat(movingDummy.location.getLat() + 0.001);
		}
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		Log.e("Thread", ex.getMessage());
	}
}
